package gsalaun1.configurationpropertiespoc.service

import gsalaun1.configurationpropertiespoc.config.IPocProperties
import org.springframework.stereotype.Service

@Service
class PocService(val pocProperties: IPocProperties) {
    fun run() {
        println("First field : ${pocProperties.field}")
        println("Second field in first inner class : ${pocProperties.firstInnerClass.field}")
        println("Optional field optional and mandatory inner class : ${pocProperties.fourthInnerClass.optionalField}")
    }
}
