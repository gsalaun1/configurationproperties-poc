package gsalaun1.configurationpropertiespoc.config

import org.hibernate.validator.constraints.Length
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.validation.annotation.Validated
import javax.validation.Valid

@Configuration
@ConfigurationProperties(prefix = "poc")
@Validated
class PocProperties : IPocProperties {
    override lateinit var field: String

    override val firstInnerClass = IPocProperties.InnerClass()

    val secondInnerClass = IPocProperties.InnerClass()

    @Valid
    override val fourthInnerClass = ClassWithValidation()

    class ClassWithValidation : IPocProperties.InnerInterface {
        @Length(min = 10)
        override lateinit var optionalField: String
    }
}
