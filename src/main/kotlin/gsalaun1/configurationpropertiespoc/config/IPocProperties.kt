package gsalaun1.configurationpropertiespoc.config

interface IPocProperties {

    val field: String

    val firstInnerClass: InnerClass

    val fourthInnerClass: InnerInterface

    class InnerClass {
        lateinit var field: String
    }

    interface InnerInterface {
        var optionalField: String
    }
}
