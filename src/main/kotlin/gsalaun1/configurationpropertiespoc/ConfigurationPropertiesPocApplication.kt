package gsalaun1.configurationpropertiespoc

import gsalaun1.configurationpropertiespoc.service.PocService
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class ConfigurationPropertiesPocApplication {
    @Bean
    fun init(pocService: PocService) = CommandLineRunner {
        pocService.run()
    }
}

fun main(args: Array<String>) {
    runApplication<ConfigurationPropertiesPocApplication>(*args)
}
